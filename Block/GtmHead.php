<?php


namespace M21\GoogleTagManager\Block;

class GtmHead extends \Magento\Framework\View\Element\Template
{

    protected $scopeConfig;

    /**
     * GtmHead constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function gtmId()
    {
        return $this->scopeConfig->getValue('settings/settings/gtmid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue('settings/settings/status', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
