## Magento 2 Google Tag Manager

Menedżer tagów Google (GTM) to przyjazne dla użytkownika rozwiązanie, które upraszcza proces dodawania, edytowania i zarządzania tagami JavaScript w sklepie Magento.

### Cechy
* Szybka i prosta instalacja za pomocą composer
* Szybka i łatwa konfiguracja
* Możliwość wyłączenia modułu z poziomu panelu magento

#### Instalacja

##### Używając Composer (polecana)
```
php composer.phar require m21/module-googletagmanager:dev-master

php bin/magento setup:upgrade;
php bin/magento cache:flush;
php bin/magento cache:enable
```

#### Konfiguracja

##### Jak skonfigurować Menedżera tagów Google
Zaloguj się do administratora Magento 2, następnie przejdź do Sklepów -> Konfiguracja -> Google Tag Manager -> Ustawienia i wprowadź ID swojego konta GTM.
Włącz moduł i zapisz ustawienia.


## Kontakt
````
Sklepy Magento
21w.pl Sylwester Kowal
tel.: +48 608012047
